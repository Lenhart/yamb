package rs.etf.lj080035d.pmuyamb;

import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.ColumnType;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.FieldType;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MyYambApp extends Application {

	private static Context context;
	
	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();
	}
	
	public static Context getContext() {
		return context;
	}
	
	public static boolean isShakeEnabled() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
		return prefs.getBoolean("shake_enabled", false);
	}
	
	public static boolean isReadyDialogEnabled() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
		return prefs.getBoolean("readyDialog", true);
	}
	
	public static boolean isColumnEnabled(ColumnType column) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
		return prefs.getBoolean("column_code_" + column.getCode(), true);
	}
	
	public static boolean isFieldEnabled(FieldType field) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
		return prefs.getBoolean(field.getDescription(), true);
	}
	
	public static int getReplaySpeed() {
		String replaySpeedString = PreferenceManager.getDefaultSharedPreferences(MyYambApp.getContext()).getString("replay_speed", "1000");
		int replaySpeed = 1000;
		try {
			replaySpeed = Integer.parseInt(replaySpeedString);
		} catch (Exception e) {
			// parsing failed, just continue with default value
		}
		return replaySpeed;
	}
	
	public static double getShakeSensitivity() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MyYambApp.getContext());
		double shakeSensitivity = 12;
		try {
			shakeSensitivity = Double.parseDouble(prefs.getString("shake_param", "12"));
		} catch (Exception e) {
			
		}
		return shakeSensitivity;
	}
	
	public static double getShakeSensitivity2() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MyYambApp.getContext());
		double shakeSensitivity = 2;
		try {
			shakeSensitivity = Double.parseDouble(prefs.getString("shake_param2", "5"));
		} catch (Exception e) {
			
		}
		return shakeSensitivity;
	}
	
	public static double getShakeSensitivity3() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MyYambApp.getContext());
		double shakeSensitivity = 500;
		try {
			shakeSensitivity = Double.parseDouble(prefs.getString("shake_param3", "500"));
		} catch (Exception e) {
			
		}
		return shakeSensitivity;
	}
}
