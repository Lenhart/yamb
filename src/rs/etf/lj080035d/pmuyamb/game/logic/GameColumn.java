package rs.etf.lj080035d.pmuyamb.game.logic;

import java.util.HashMap;
import java.util.List;

import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.FieldType;

public class GameColumn {

	private HashMap<FieldType, Integer> data;
	
	public GameColumn(List<FieldType> fields) {
		this.data = new HashMap<FieldType, Integer>();
	}
	
	public Integer getValue(FieldType field) {
		return data.get(field);
	}
	
	public void setValue(FieldType field, Integer value) {
		data.put(field, value);
	}
	
}
