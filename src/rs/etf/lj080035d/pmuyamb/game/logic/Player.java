package rs.etf.lj080035d.pmuyamb.game.logic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;

import rs.etf.lj080035d.pmuyamb.game.DatabaseUtil;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.ColumnType;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.FieldType;

public class Player {
	
	public static class MoveData implements Serializable {
		private static final long serialVersionUID = -7041296752400991195L;
		private ColumnType column;
		private FieldType field;
		private int value;
		
		public MoveData() {
			this(1, 1, 0);
		}
		
		public MoveData(ColumnType column, FieldType field, int value) {
			this.column = column;
			this.field = field;
			this.value = value;
		}
		
		public MoveData(int column, int field, int value) {
			this(ColumnType.getByCode(column), FieldType.getByCode(field), value);
		}

		public ColumnType getColumn() {
			return column;
		}

		public void setColumn(ColumnType column) {
			this.column = column;
		}

		public FieldType getField() {
			return field;
		}

		public void setField(FieldType field) {
			this.field = field;
		}

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}
		
	}

	private String name;
	
	public String getName() {
		return this.name;
	}
	
	private int nonHandFillFields;
	private int handFields;
	private List<MoveData> moves;
	
	private HashMap<ColumnType, GameColumn> dataMap;
	
	public Player(String name, List<ColumnType> columns, List<FieldType> fields) {
		this.name = name;
		this.dataMap = new HashMap<ColumnType, GameColumn>(columns.size());
		this.moves = new LinkedList<MoveData>();
		
		GameManager gm = GameManager.getInstance();
		this.handFields = 0;
		for (ColumnType column : gm.getColumns()) {
			if (column == ColumnType.Hand) {
				this.handFields = gm.getFillFields();
				break;
			}
		}
		this.nonHandFillFields = gm.getFillFields() * gm.getNonHandColumns();
		
		for (int i = 0; i < columns.size(); i++) {
			ColumnType type = columns.get(i);
			dataMap.put(type, new GameColumn(fields));
		}
	}
	
	public Integer getValue(ColumnType column, FieldType field) {
		GameColumn data = dataMap.get(column);
		if (data != null) {
			return data.getValue(field);
		}
		
		return null;
	}
	
	public void setValue(ColumnType column, FieldType field, Integer value) {
		GameColumn data = dataMap.get(column);
		if (data != null) {
			if (column != ColumnType.Hand) {
				this.nonHandFillFields--;
			} else {
				this.handFields--;
			}
			moves.add(new MoveData(column, field, value));
			data.setValue(field, value);
		}
	}
	
	public void dump(Context context, int gamePkey) {
		for (MoveData move : this.moves) {
			DatabaseUtil.insert(context, gamePkey, move);
		}
	}
	
	private Integer score = -1;
	
	public void setScore(Integer score) {
		this.score = score;
	}
	
	public int getScore() {
		if (this.score != null) {
			return this.score;
		}
		return -1;
	}
	
	public boolean hasNonHandFields() {
		return this.nonHandFillFields > 0;
	}
	
	public boolean hasMoreMoves() {
		return (this.nonHandFillFields > 0) || (this.handFields > 0);
	}
	
}
