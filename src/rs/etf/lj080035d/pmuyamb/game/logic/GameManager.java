package rs.etf.lj080035d.pmuyamb.game.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import rs.etf.lj080035d.pmuyamb.MyYambApp;
import rs.etf.lj080035d.pmuyamb.Threading;
import rs.etf.lj080035d.pmuyamb.game.DatabaseUtil;
import rs.etf.lj080035d.pmuyamb.game.ui.DiceView;
import rs.etf.lj080035d.pmuyamb.game.ui.GameActivity;
import rs.etf.lj080035d.pmuyamb.game.ui.YambDialogs;

public class GameManager {

	private static final GameManager INSTANCE = new GameManager();

	public enum ColumnType {
		Downwards(1),
		Random(2),
		Upwards(3, false),
		Medial(4, false),
		Hand(5, false),
		Announce(6, false),
		Sums(7);

		private ColumnType(int code) {
			this(code, true);
		}
		
		private ColumnType(int code, boolean alwaysEnabled) {
			this.code = code;
			this.alwaysEnabled = alwaysEnabled;
		}
		
		private boolean alwaysEnabled;

		private int code;

		public int getCode() {
			return this.code;
		}

		private static final HashMap<Integer, ColumnType> mapByCode = new HashMap<Integer, ColumnType>();
		private static int minCode;
		private static int maxCode;

		static {
			minCode = Integer.MAX_VALUE;
			maxCode = Integer.MIN_VALUE;
			for (ColumnType column : ColumnType.values()) {
				mapByCode.put(column.getCode(), column);
				if (column.getCode() < minCode) {
					minCode = column.getCode();
				}
				if (column.getCode() > maxCode) {
					maxCode = column.getCode();
				}
			}
		}

		public boolean addsToNonHand() {
			return this != Sums && this != Hand;
		}

		public static ColumnType getByCode(int code) {
			return mapByCode.get(code);
		}

		public static int getMinCode() {
			return minCode;
		}

		public static int getMaxCode() {
			return maxCode;
		}
		
		public boolean isEnabled() {
			if (this.alwaysEnabled) {
				return true;
			}
			
			return MyYambApp.isColumnEnabled(this);
		}
	}

	public enum FieldType {
		N1(1, "1"),
		N2(2, "2"),
		N3(3, "3"),
		N4(4, "4"),
		N5(5, "5"),
		N6(6, "6"),
		NSUM(7, "SUM"),
		Max(8, "MAX"),
		Min(9, "MIN"),
		MinMaxSum(10, "SUM"),
		Triling(11, "Triling", false),
		Straight(12, "Straight", false),
		Full(13, "Full", false),
		Poker(14, "Poker", false),
		Yamb(15, "Yamb", false),
		TotalSum(16, "SUM");

		private FieldType(int code, String description) {
			this(code, description, true);
		}
		
		private FieldType(int code, String description, boolean alwaysEnabled) {
			this.code = code;
			this.description = description;
			this.alwaysEnabled = alwaysEnabled;
		}
		
		private boolean alwaysEnabled;

		public boolean isEnabled() {
			if (this.alwaysEnabled) {
				return true;
			}
			
			return MyYambApp.isFieldEnabled(this);
		}

		private int code;

		public int getCode() {
			return this.code;
		}

		private static final HashMap<Integer, FieldType> mapByCode = new HashMap<Integer, FieldType>();
		private static int minCode;
		private static int maxCode;

		static {
			minCode = Integer.MAX_VALUE;
			maxCode = Integer.MIN_VALUE;
			for (FieldType field : FieldType.values()) {
				mapByCode.put(field.getCode(), field);
				if (field.getCode() < minCode) {
					minCode = field.getCode();
				}
				if (field.getCode() > maxCode) {
					maxCode = field.getCode();
				}
			}
		}

		public static FieldType getByCode(int code) {
			return mapByCode.get(code);
		}

		public static int getMinCode() {
			return minCode;
		}

		public static int getMaxCode() {
			return maxCode;
		}

		private String description;

		public String getDescription() {
			return this.description;
		}

		public boolean isAutoFilled() {
			if (this == FieldType.NSUM || this == FieldType.MinMaxSum || this == FieldType.TotalSum) {
				return true;
			}
			return false;
		}
	}

	private static final int MAX_ROLLS = 3;

	private List<Player> players;
	private boolean replay;

	private List<DiceView> dices;

	private long gameStarted;
	private long gameEnded;

	private int rollCounter;

	private int currentPlayerId;

	private GameActivity updater;

	public int getRollCounter() {
		return rollCounter;
	}

	public void incRollCounter() {
		this.rollCounter++;
	}

	public boolean rollAllowed() {
		boolean handsGood = getCurrentPlayer().hasNonHandFields() || getRollCounter() < 1;
		return handsGood && (getRollCounter() < MAX_ROLLS);
	}
	private Integer nonHandColumns = null;
	private Integer fillFields = null;
	
	private List<ColumnType> columns;
	private List<FieldType> fields;

	public int getNonHandColumns() {
		if (this.nonHandColumns == null) {
			int cc = 0;
			for (ColumnType column : getColumns()) {
				if (column.addsToNonHand()) {
					cc++;
				}
			}
			this.nonHandColumns = cc;
		}
		return this.nonHandColumns;
	}

	public int getFillFields() {
		if (this.fillFields == null) {
			int fc = 0;
			for (FieldType field : getFields()) {
				if (!field.isAutoFilled()) {
					fc++;
				}
			}
			this.fillFields = fc;
		}
		return this.fillFields;
	}

	private FieldType announcedField;

	public boolean canAnnounce() {
		return rollCounter <= 1;
	}

	public void announce(FieldType field) {
		this.announcedField = field;
	}

	public boolean isAnnounced() {
		return this.announcedField != null;
	}

	public FieldType getAnnounced() {
		return this.announcedField;
	}

	public void reset(List<String> newPlayers, List<ColumnType> columns, List<FieldType> fields) {
		this.players = new ArrayList<Player>();
		this.replay = columns != null || fields != null;
		this.columns = columns;
		this.fields = fields;
		for (String playerName : newPlayers) {
			this.players.add(new Player(playerName, getColumns(), getFields()));
		}
		this.rollCounter = 0;
		this.currentPlayerId = 0;
		this.announcedField = null;
		this.updater = null;
		this.nonHandColumns = null;
		this.fillFields = null;
		this.gameStarted = System.currentTimeMillis();
		this.gameEnded = System.currentTimeMillis();
		this.locked = false;
	}

	public void finishGame() {
		if (!this.replay) {
			this.gameEnded = System.currentTimeMillis();
			dump();
		}
		this.updater.finish();
	}

	private GameManager() {
		
	}

	public static GameManager getInstance() {
		return INSTANCE;
	}
	
	public void resetColumnsAndFields()
	{
		this.columns = null;
		this.fields = null;
	}

	public List<ColumnType> getColumns() {
		if (this.columns != null) {
			return this.columns;
		}
		List<ColumnType> columnsPlayed = new LinkedList<ColumnType>();
		for (ColumnType columnType : ColumnType.values()) {
			if (columnType.isEnabled()) {
				columnsPlayed.add(columnType);
			}
		}
		return columnsPlayed;
	}

	public List<FieldType> getFields() {
		if (this.fields != null) {
			return this.fields;
		}
		List<FieldType> fields = new LinkedList<FieldType>();
		for (FieldType field : FieldType.values()) {
			if (field.isEnabled()) {
				fields.add(field);
			}
		}
		
		return fields;
	}

	public int getDiceAmount() {
		return 6;
	}

	public List<DiceView> getDices() {
		return this.dices;
	}

	public void setDices(List<DiceView> dices) {
		this.dices = dices;
	}

	public void moveExecuted() {
		this.locked = true;
		this.currentPlayerId = (this.currentPlayerId + 1) % this.players.size();
		if (!getCurrentPlayer().hasMoreMoves() && this.currentPlayerId == 0) {
			int maxPoints = Integer.MIN_VALUE;
			Player winner = null;
			for (Player player : this.players) {
				if (player.getScore() > maxPoints) {
					maxPoints = player.getScore();
					winner = player;
				}
			}
			if (!this.replay) {
				YambDialogs.showWinner(this.updater, winner);
			} else {
				finishGame();
			}
		} else {
			if (!this.replay && MyYambApp.isReadyDialogEnabled()) {
				YambDialogs.getReady(this.updater, this);
			} else {
				update();
			}
		}
	}
	
	private boolean locked = false;
	
	public boolean isLocked()
	{
		return this.locked;
	}
	
	public void update()
	{
		this.announcedField = null;
		this.locked = false;
		this.rollCounter = 0;
		unmarkAllDices();
		this.updater.update();
	}

	public void unmarkAllDices() {
		for (DiceView dice : dices) {
			dice.setMarked(false);
		}
	}

	public Player getCurrentPlayer() {
		return this.players.get(this.currentPlayerId);
	}

	public void setUpdater(GameActivity updater) {
		this.updater = updater;
	}

	public void dump() {
		long duration = this.gameEnded - this.gameStarted;
		Threading.storeGame(this.players, duration, this.gameStarted);
	}

}
