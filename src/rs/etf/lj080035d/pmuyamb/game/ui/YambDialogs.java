package rs.etf.lj080035d.pmuyamb.game.ui;

import rs.etf.lj080035d.pmuyamb.MyYambApp;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager;
import rs.etf.lj080035d.pmuyamb.game.logic.Player;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;

public class YambDialogs {

	public static void getReady(Activity activity, final GameManager gm) {
		final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
		alert.setMessage(GameManager.getInstance().getCurrentPlayer().getName());
		alert.setTitle("Get ready");
		alert.setCancelable(false);
		alert.setPositiveButton("Ready!", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				gm.update();
			}
		});
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				alert.show();
			}
		}, MyYambApp.getReplaySpeed() / 2);
	}
	
	public static void showWinner(Activity activity, Player player) {
		AlertDialog.Builder alert = new AlertDialog.Builder(activity);
		alert.setMessage(player.getName() + " has won with " + player.getScore() + " points!");
		alert.setTitle("Victory!");
		alert.setPositiveButton("Yay!", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				GameManager.getInstance().finishGame();
			}
		});
		alert.show();
	}
	
	public static void backYouSure(final Activity activity) {
		AlertDialog.Builder alert = new AlertDialog.Builder(activity);
		alert.setMessage("Are you sure you want to leave the game?");
		alert.setTitle("Going back?");
		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				activity.finish();
			}
		});
		alert.setNegativeButton("No", null);
		alert.show();
	}
}
