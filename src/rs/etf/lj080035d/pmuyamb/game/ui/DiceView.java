package rs.etf.lj080035d.pmuyamb.game.ui;

import java.util.Random;

import rs.etf.lj080035d.pmuyamb.R;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager;

import android.content.Context;
import android.view.View;
import android.widget.Button;

public class DiceView extends Button {
	
	public static final int MIN_VALUE = 1;
	public static final int MAX_VALUE = 6;
	
	private int value;
	private Random random;
	private boolean isMarked;

	public DiceView(Context context) {
		super(context);
		
		this.random = new Random();
		roll();
		setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (GameManager.getInstance().getRollCounter() > 0) {
					toggleMarked();
				}
			}
		});
	}
	
	public void roll() {
		if (!isMarked) {
			this.value = this.random.nextInt(MAX_VALUE - MIN_VALUE + 1) + MIN_VALUE;
			updateBackground(this.value);
		}
	}
	
	private void updateBackground(int value) {
		int imageId;
		
		if (this.isMarked) {
			switch (value) {
			case 1:
				imageId = R.drawable.marked_dice_1;
				break;
			case 2:
				imageId = R.drawable.marked_dice_2;
				break;
			case 3:
				imageId = R.drawable.marked_dice_3;
				break;
			case 4:
				imageId = R.drawable.marked_dice_4;
				break;
			case 5:
				imageId = R.drawable.marked_dice_5;
				break;
			case 6:
				imageId = R.drawable.marked_dice_6;
				break;
			default:
				imageId = R.drawable.down;
			}
		} else {
			switch (value) {
			case 1:
				imageId = R.drawable.dice_1;
				break;
			case 2:
				imageId = R.drawable.dice_2;
				break;
			case 3:
				imageId = R.drawable.dice_3;
				break;
			case 4:
				imageId = R.drawable.dice_4;
				break;
			case 5:
				imageId = R.drawable.dice_5;
				break;
			case 6:
				imageId = R.drawable.dice_6;
				break;
			default:
				imageId = R.drawable.down;
			}
		}
		
		setBackgroundResource(imageId);
	}

	public int getValue() {
		return value;
	}
	
	public void setMarked(boolean marked) {
		this.isMarked = marked;
		updateBackground(this.value);
	}
	
	public void toggleMarked() {
		setMarked(!isMarked);
	}
}
