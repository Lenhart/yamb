package rs.etf.lj080035d.pmuyamb.game.ui;

import java.util.HashMap;
import java.util.Map;

import rs.etf.lj080035d.pmuyamb.R;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.ColumnType;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.FieldType;
import android.content.Context;
import android.widget.Button;

public class FieldButton extends Button {
	
	private ColumnType column;
	private FieldType field;
	private GameManager gm;

	public FieldButton(Context context, ColumnType column, FieldType field) {
		super(context);
		this.setMinimumWidth(88);
		this.setMinimumHeight(40);
		this.setWidth(88);
		this.setHeight(40);
		this.setTextSize(10);
		
		this.column = column;
		this.field = field;
		this.gm = GameManager.getInstance();
	}
	
	public void press() {
		if (this.gm.getRollCounter() == 0) {
			// TODO dialog to remind rolling dice
			return;
		}
		if (this.gm.isLocked()) {
			// Buttons could be disabled now, but why spend time updating them...
			return;
		}
		
		if (this.column == ColumnType.Announce) {
			if (!this.gm.isAnnounced() || this.field != this.gm.getAnnounced()) {
				this.gm.announce(this.field);
				return;
			}
		}
		
		Integer calculatedValue = calculateValue(this.column, this.field);
		if (calculatedValue == null) {
			calculatedValue = 0;
		}
		this.gm.getCurrentPlayer().setValue(this.column, this.field, calculatedValue);
		update();
		this.gm.moveExecuted();
	}
	
	public void update() {
		if (this.column == ColumnType.Sums && this.field == FieldType.TotalSum) {
			int x = 0;
			x = x + 1;
		}
		
		if (this.column != null) {
			if (this.field != null) {
				// this is a regular field
				Integer value = this.gm.getCurrentPlayer().getValue(this.column, this.field);
				if (value != null) {
					setText(value.toString());
					enable(false, false);
				} else {
					if (this.field.isAutoFilled()) {
						enable(false, false);
						value = calculateValue(this.column, this.field);
						if (value != null) {
							setText(value.toString());
						} else {
							setText("");
						}
					} else {
						setText("");
						enable(decideEnabled(), false);
					}
				}
			} else {
				// this is top row
				enable(false, true);
				setColumnImage();
			}
		} else {
			// this is the left column, it's supposed to be filled with explanations
			enable(false, true);
			if (this.field != null) {
				setText(this.field.getDescription());
			} else {
				// this is the field in top left corner, we do nothing since it's supposed to be empty
				setText("");
			}
		}
	}
	
	private void enable(boolean enable, boolean frame) {
		setEnabled(enable);
		if (enable) {
			setBackgroundResource(R.drawable.light);
		} else {
			boolean dark = false;
			dark = dark || frame;
			dark = dark || this.column == ColumnType.Sums;
			dark = dark || (this.field == null || this.field.isAutoFilled());
			if (dark) {
				setBackgroundResource(R.drawable.dark);
			} else {
				setBackgroundResource(R.drawable.middle);
			}
		}
	}
	
	private void setColumnImage() {
		int imageId;
		
		switch (this.column) {
		case Downwards:
			imageId = R.drawable.down;
			break;
		case Upwards:
			imageId = R.drawable.up;
			break;
		case Random:
			imageId = R.drawable.free;
			break;
		case Medial:
			imageId = R.drawable.medial;
			break;
		case Hand:
			imageId = R.drawable.hand;
			break;
		case Announce:
			imageId = R.drawable.announce;
			break;
		case Sums:
			setText(FieldType.NSUM.getDescription());
			return;
		default:
			return;
		}
		setText("");
		setBackgroundResource(imageId);
	}
	
	private boolean decideEnabled() {
		switch (this.column) {
		case Downwards:
			return !this.gm.isAnnounced() && isNeighbourTaken(false, null);
		case Upwards:
			return !this.gm.isAnnounced() && isNeighbourTaken(true, null);
		case Random:
			return !this.gm.isAnnounced();
		case Medial:
			boolean downwards = this.field.getCode() <= FieldType.Max.getCode();
			FieldType specialBoundary = downwards ? FieldType.Min : FieldType.Max;
			return !this.gm.isAnnounced() && isNeighbourTaken(downwards, specialBoundary);
		case Hand:
			return !this.gm.isAnnounced() && this.gm.getRollCounter() <= 1;
		case Announce:
			if (this.gm.canAnnounce() || this.gm.getAnnounced() == this.field) {
				return true;
			}
			return false;
		case Sums:
			return false;
		default:
			throw new IllegalArgumentException("Unknown column type");
		}
	}
	
	private boolean isNeighbourTaken(boolean downwards, FieldType specialBoundary) {
		int increment = downwards ? 1 : - 1;
		FieldType otherField = this.field;
		while (otherField != null && (this.field == otherField || otherField.isAutoFilled() || !this.gm.getFields().contains(otherField))) {
			otherField = FieldType.getByCode(otherField.getCode() + increment);
		}
		if (otherField == null || otherField == specialBoundary) {
			return true;
		}
		 return gm.getCurrentPlayer().getValue(this.column, otherField) != null;
	}
	
	private Integer calculateSumsValue(FieldType field) {
		switch (field) {
		case NSUM:
			return rowSum(field);
		case MinMaxSum:
			return rowSum(field);
		case TotalSum:
			Integer totalScore = rowSum(field);
			this.gm.getCurrentPlayer().setScore(totalScore);
			return totalScore;
		default:
			return null;
		}
	}
	
	private Integer rowSum(FieldType field) {
		Integer sum = null;
		for (ColumnType column : this.gm.getColumns()) {
			if (column == ColumnType.Sums) {
				continue;
			}
			Integer addition = calculateValue(column, field);
			if (addition != null) {
				if (sum == null) {
					sum = addition;
				} else {
					sum += addition;
				}
			}
		}
		return sum;
	}
	
	private Integer calculateValue(ColumnType column, FieldType field) {
		if (column == ColumnType.Sums) {
			return calculateSumsValue(field);
		}
		
		Map<Integer, Integer> map = mapCounts();
		Integer value1, value2;
		
		switch (field) {
		case N1:
			return map.get(1);
		case N2:
			return 2 * map.get(2);
		case N3:
			return 3 * map.get(3);
		case N4:
			return 4 * map.get(4);
		case N5:
			return 5 * map.get(5);
		case N6:
			return 6 * map.get(6);
		case NSUM:
			value1 = 0;
			for (int i = 1; i <= 6; i++) {
				FieldType numField = FieldType.getByCode(FieldType.N1.getCode() - 1 + i);
				value2 = this.gm.getCurrentPlayer().getValue(column, numField);
				if (value2 == null) {
					return null;
				}
				value1 += value2;
			}
			if (value1 >= 60) {
				value1 += 30;
			}
			return value1;
		case Max:
			return sum() - findMinValue();
		case Min:
			return sum() - findMaxValue();
		case MinMaxSum:
			value1 = this.gm.getCurrentPlayer().getValue(column, FieldType.Min);
			value2 = this.gm.getCurrentPlayer().getValue(column, FieldType.Max);
			Integer ones = this.gm.getCurrentPlayer().getValue(column, FieldType.N1);
			if (value1 == null || value2 == null || ones == null) {
				return null;
			}
			return Math.abs(ones * (value2 - value1));
		case Triling:
			value1 = valueWithEncounters(map, 3, null);
			if (value1 == null) {
				return null;
			}
			return value1 * 3 + 20;
		case Straight:
			boolean oneOrSix = map.get(1) >= 1 || map.get(6) >= 1;
			if (oneOrSix) {
				for (int i = 2; i <= 5; i++) {
					if (map.get(i) < 1) {
						return null;
					}
				}
				return 66 - (this.gm.getRollCounter() - 1) * 10;
			}
			return null;
		case Full:
			value1 = valueWithEncounters(map, 3, null);
			if (value1 == null) {
				return null;
			}
			value2 = valueWithEncounters(map, 2, value1);
			if (value2 == null) {
				return null;
			}
			return value1 * 3 + value2 * 2 + 30;
		case Poker:
			value1 = valueWithEncounters(map, 4, null);
			if (value1 == null) {
				return null;
			}
			return value1 * 4 + 40;
		case Yamb:
			value1 = valueWithEncounters(map, 5, null);
			if (value1 == null) {
				return null;
			}
			return value1 * 5 + 50;
		case TotalSum:
			value1 = 0;
			value1 = add(value1, column, FieldType.NSUM);
			value1 = add(value1, column, FieldType.MinMaxSum);
			value1 = add(value1, column, FieldType.Triling);
			value1 = add(value1, column, FieldType.Straight);
			value1 = add(value1, column, FieldType.Full);
			value1 = add(value1, column, FieldType.Poker);
			value1 = add(value1, column, FieldType.Yamb);
			if (value1 == 0) {
				return null;
			}
			return value1;

		default:
			throw new IllegalArgumentException();
		}
	}
	
	private int countDicesWith(int value) {
		int count = 0;
		for (DiceView dice : this.gm.getDices()) {
			if (dice.getValue() == value) {
				count++;
			}
		}
		return count;
	}
	
	private Map<Integer, Integer> mapCounts() {
		Map<Integer, Integer> result = new HashMap<Integer, Integer>();
		for (int i = DiceView.MIN_VALUE; i <= DiceView.MAX_VALUE; i++) {
			result.put(i, countDicesWith(i));
		}
		return result;
	}
	
	private Integer valueWithEncounters(Map<Integer, Integer> map, int encounters, Integer ignore) {
		for (Integer i = DiceView.MAX_VALUE; i >= DiceView.MIN_VALUE; i--) {
			if (i == ignore) {
				continue;
			}
			if (map.get(i) >= encounters) {
				return i;
			}
		}
		return null;
	}
	
	private int findMinValue() {
		int min = DiceView.MAX_VALUE;
		for (DiceView dice : this.gm.getDices()) {
			if (dice.getValue() < min) {
				min = dice.getValue();
			}
		}
		return min;
	}
	
	private int findMaxValue() {
		int max = DiceView.MIN_VALUE;
		for (DiceView dice : this.gm.getDices()) {
			if (dice.getValue() > max) {
				max = dice.getValue();
			}
		}
		return max;
	}
	
	private int sum() {
		int sum = 0;
		for (DiceView dice : this.gm.getDices()) {
			sum += dice.getValue();
		}
		return sum;
	}
	
	private int add(int value, ColumnType column, FieldType field) {
		Integer addition;
		if (field.isAutoFilled()) {
			addition = calculateValue(column, field);
		} else {
			addition = this.gm.getCurrentPlayer().getValue(column, field);
		}
		if (addition == null) {
			return value;
		}
		return value + addition;
	}

}
