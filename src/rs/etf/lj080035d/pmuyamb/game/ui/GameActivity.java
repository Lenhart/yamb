package rs.etf.lj080035d.pmuyamb.game.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rs.etf.lj080035d.pmuyamb.MyYambApp;
import rs.etf.lj080035d.pmuyamb.R;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.ColumnType;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.FieldType;
import rs.etf.lj080035d.pmuyamb.game.logic.Player.MoveData;
import rs.etf.lj080035d.pmuyamb.preferences.PreferencesActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GameActivity extends ActionBarActivity implements SensorEventListener {

	private TextView playerNameText;
	private TextView rollCountText;
	private Button rollButton;

	public Button getRollButton() {
		return this.rollButton;
	}

	private List<SingleYambColumn> columns;

	private List<DiceView> dices = new LinkedList<DiceView>();

	private GameManager gm;

	private String replayPlayerName;
	private List<MoveData> moves;
	private Handler replayHandler;
	private int replayMove;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		this.playerNameText = (TextView) findViewById(R.id.playerNameText);

		if (getIntent().hasExtra("moves") && getIntent().hasExtra("playerName")) {
			this.replayPlayerName = getIntent().getExtras().getString("playerName");
			this.moves = (ArrayList<MoveData>) getIntent().getSerializableExtra("moves");
		}

		this.gm = GameManager.getInstance();

		List<FieldType> fields;
		List<ColumnType> columns;
		if (!isReplay()) {
			columns = this.gm.getColumns();
			fields = this.gm.getFields();
		} else {
			columns = analyzeColumns();
			fields = analyzeFields();
		}

		LinearLayout gameLayout = (LinearLayout) findViewById(R.id.gameLayout);
		// We add 1 to columns, because in GUI we have explanation of each column to the left.
		this.columns = new LinkedList<SingleYambColumn>();
		SingleYambColumn oneColumn = new SingleYambColumn(this, null, fields);
		this.columns.add(oneColumn);
		gameLayout.addView(oneColumn);
		for (int i = 0; i < columns.size(); i++) {
			oneColumn = new SingleYambColumn(this, columns.get(i), fields);
			this.columns.add(oneColumn);
			gameLayout.addView(oneColumn);
		}

		this.rollCountText = (TextView) findViewById(R.id.roll_count);
		this.rollButton = (Button) findViewById(R.id.roll_dice_button);

		if (!isReplay()) {
			LinearLayout diceLayout = (LinearLayout) findViewById(R.id.diceLayout);
			for (int i = 0; i < this.gm.getDiceAmount(); i++) {
				DiceView dice = new DiceView(this);
				this.dices.add(dice);
				diceLayout.addView(dice);
			}

		} else {
			List<String> players = new LinkedList<String>();
			players.add(this.replayPlayerName);
			this.gm.reset(players, columns, fields);
			this.replayMove = 0;
			this.replayHandler = new Handler();

			final int finalReplaySpeed = Math.max(100, Math.min(10000, MyYambApp.getReplaySpeed()));

			this.replayHandler.postDelayed(new Runnable() {

				@Override
				public void run() {
					MoveData move = moves.get(replayMove++);
					gm.getCurrentPlayer().setValue(move.getColumn(), move.getField(), move.getValue());
					gm.moveExecuted();
					if (gm.getCurrentPlayer().hasMoreMoves()) {
						replayHandler.postDelayed(this, finalReplaySpeed);
					}
				}
			}, finalReplaySpeed);
		}
		this.gm.setDices(this.dices);
		this.gm.setUpdater(this);
		update();
		
		lastShakeTime = System.currentTimeMillis();

		SensorManager senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);

		mAccel = 0.00f;
		mAccelCurrent = SensorManager.GRAVITY_EARTH * 10;
		mAccelLast = SensorManager.GRAVITY_EARTH * 10;
		
		this.mp = MediaPlayer.create(this, R.raw.roll_dice);
		this.mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			
			@Override
			public void onPrepared(MediaPlayer mp) {
				GameActivity.this.allowedToPlaySound = true;
			}
		});
		this.mp.setLooping(true);
//		this.mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//			@Override
//			public void onCompletion(MediaPlayer mediaPlayer) {
//				if (GameActivity.this.shakeCount > 0) {
//					GameActivity.this.shakeCount = 0;
//					
//					mp.start();
//				} else {
////					if (MyYambApp.isShakeEnabled()) {
////						diceRolled(false);
////					}
//				}
//			}
//		});
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MyYambApp.getContext());
		this.listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
			
			@Override
			public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
					String key) {
				GameActivity.this.update();
			}
		};
		preferences.registerOnSharedPreferenceChangeListener(this.listener);
	}
	
	private SharedPreferences.OnSharedPreferenceChangeListener listener;
	private boolean allowedToPlaySound = false;
//	@Override
//	public void onDestroy()
//	{	// for some reason release throw exception...
//		if (this.mp != null) {
//			this.mp.release();
//			this.mp = null;
//		}
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, PreferencesActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private List<ColumnType> analyzeColumns() {
		List<ColumnType> columns = new ArrayList<ColumnType>();

		for (MoveData move : this.moves) {
			if (!columns.contains(move.getColumn())) {
				columns.add(move.getColumn());
			}
		}
		columns.add(ColumnType.Sums);

		return columns;
	}

	private List<FieldType> analyzeFields() {
		List<FieldType> savedFields = new ArrayList<FieldType>();
		for (MoveData move : this.moves) {
			if (!savedFields.contains(move.getField())) {
				savedFields.add(move.getField());
			}
		}

		List<FieldType> fields = new ArrayList<FieldType>();
		for (FieldType field : FieldType.values()) {
			if (field.isAutoFilled() || savedFields.contains(field)) {
				fields.add(field);
			}
		}
		return fields;
	}

	private boolean isReplay() {
		return this.moves != null;
	}
	
	private void playShakeSound(boolean ignoreIfPLays) {
//		if (this.mp.isPlaying()) {
//			if (!ignoreIfPLays) {
//				this.shakeCount++;
//			}
//		} else {
//			this.shakeCount = 0;
//			if (this.allowedToPlay) {
//				this.mp.start();
//			}
//		}
	}
	
	private void diceRolled(boolean dontCareIfPlays)
	{
		if (this.gm.isLocked()) {
			// Alternative: update all buttons.
			return;
		}
		if (!this.gm.rollAllowed()) {
			return;
		}
		for (DiceView dice : dices) {
			dice.roll();
		}
		this.gm.incRollCounter();
		if (dontCareIfPlays) {
			playShakeSound(dontCareIfPlays);
		}
		update();
	}

	public void rollDiceButtonPressed(View view) {
		if (!MyYambApp.isShakeEnabled()) {
			diceRolled(true);
		}
	}

	public void update() {
		this.playerNameText.setText(getString(R.string.on_turn) + " " + this.gm.getCurrentPlayer().getName());
		if (!isReplay()) {
			if (this.gm.getRollCounter() > 0) {
				this.rollCountText.setText(String.format(getString(R.string.roll_count), this.gm.getRollCounter()));
			} else {
				this.rollCountText.setText("");
			}
			if (MyYambApp.isShakeEnabled()) {
				this.rollButton.setVisibility(View.INVISIBLE);
			} else {
				this.rollButton.setVisibility(View.VISIBLE);
			}
			this.rollButton.setEnabled(this.gm.rollAllowed());
		} else {
			this.rollCountText.setText("");
			this.rollButton.setVisibility(View.INVISIBLE);
			this.rollButton.setEnabled(false);
		}

		for (SingleYambColumn oneColumn : this.columns) {
			oneColumn.update();
		}
	}

	private float mAccel;
	private float mAccelCurrent;
	private float mAccelLast;
	private long lastShakeTime;

	@Override
	public void onSensorChanged(SensorEvent se) {
		if (!MyYambApp.isShakeEnabled()) {
			return;
		}
		
		double x = se.values[0] * 10;
		double y = se.values[1] * 10;
		double z = se.values[2] * 10;
		
		mAccelLast = mAccelCurrent;
		mAccelCurrent = (float) Math.sqrt(x*x + y*y + z*z);
		float delta = mAccelCurrent - mAccelLast;
		// Low cut filter
		mAccel = mAccel * 0.9f + delta;

		double shakeSensitivity = MyYambApp.getShakeSensitivity();
		if (mAccel > shakeSensitivity) {
			if (this.gm.rollAllowed()) {
				if (this.allowedToPlaySound) {
					if (!this.mp.isPlaying()) {
//						try {
////							this.mp.prepare();
							this.mp.start();
//						} catch (IllegalStateException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						} catch (IOException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
					}
				}
				for (DiceView dice : this.gm.getDices()) {
					dice.roll();
				}
			}
			this.lastShakeTime = System.currentTimeMillis();
			this.didShake = true;
		} else if (mAccel < MyYambApp.getShakeSensitivity2()) {
			if (didShake && System.currentTimeMillis() - this.lastShakeTime > MyYambApp.getShakeSensitivity3()) {
				this.shakeCount = 0;
				this.lastShakeTime = System.currentTimeMillis();
				this.didShake = false;
				if (this.mp.isPlaying()) {
					this.mp.stop();
					try {
						this.mp.prepare();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				diceRolled(false);
			}
		}
	}
	
	private boolean didShake = false;
	
	@Override
	public void onBackPressed() {
		YambDialogs.backYouSure(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		SensorManager senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		senSensorManager.unregisterListener(this);
	}
	
	@Override
	public void onResume() {
		SensorManager senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);
		super.onResume();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Nothing needed here...
	}
	
	private int shakeCount = 0;
	
	private MediaPlayer mp;
}
