package rs.etf.lj080035d.pmuyamb.game.ui;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rs.etf.lj080035d.pmuyamb.R;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class PrepareGameActivity extends ActionBarActivity {

	private List<EditText> editTexts = new ArrayList<EditText>();
	private Button button;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prepare_game);

		this.button = (Button) findViewById(R.id.startButton);
		addAnotherPlayer();
		updateStartButton();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return false;
	}

	private void addAnotherPlayer()
	{
		final LinearLayout layout = (LinearLayout) findViewById(R.id.playerNamesLayout);
		final EditText newField = new EditText(this);
		newField.setSingleLine(true);
		newField.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		newField.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.toString().trim().isEmpty())
				{
					if (getNumberOfNames(false) > 1) {
						editTexts.remove(newField);
						layout.removeView(newField);
					}
				} else {
					if (getNumberOfNames(false) == 0) {
						addAnotherPlayer();
					}
				}

				updateStartButton();
			}
		});
		layout.addView(newField);
		editTexts.add(newField);
	}

	private int getNumberOfNames(boolean valid)
	{
		int cnt = 0;
		for (EditText editText : editTexts)
		{
			if (isEligible(editText) == valid) {
				cnt++;
			}
		}

		return cnt;
	}

	private boolean isEligible(EditText editText)
	{
		if (editText != null)
		{
			return !editText.getText().toString().trim().isEmpty();
		}

		return false;
	}

	private void updateStartButton()
	{
		button.setEnabled(getNumberOfNames(true) > 0);
	}

	public void startButtonPressed(View view)
	{
		GameManager.getInstance().resetColumnsAndFields();
		List<String> playerNames = new LinkedList<String>();
		for (EditText editText : editTexts) {
			if (isEligible(editText)) {
				playerNames.add(editText.getText().toString());
			}
		}
		
		GameManager.getInstance().reset(playerNames, null, null);
		
		Intent intent = new Intent(this, GameActivity.class);
		startActivity(intent);
		this.finish();
	}
}
