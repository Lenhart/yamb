package rs.etf.lj080035d.pmuyamb.game.ui;

import java.util.LinkedList;
import java.util.List;

import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.ColumnType;
import rs.etf.lj080035d.pmuyamb.game.logic.GameManager.FieldType;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

public class SingleYambColumn extends LinearLayout {
	
	public List<FieldButton> buttons;

	public SingleYambColumn(Context context, ColumnType column, List<FieldType> fields) {
		super(context);
		this.setOrientation(VERTICAL);
		
		this.buttons = new LinkedList<FieldButton>();
		// We add 1 to rows, because in GUI we have explanation of the column at the top.
		FieldButton button = newButton(context, column, null);
		this.addView(button);
		buttons.add(button);
		for (int i = 0; i < fields.size(); i++) {
			button = newButton(context, column, fields.get(i));
			this.addView(button);
			buttons.add(button);
		}
	}
	
	private FieldButton newButton(Context context, ColumnType column, FieldType field)
	{
		final FieldButton button = new FieldButton(context, column, field);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				button.press();
			}
		});
		
		return button;
	}
	
	public void update() {
		for (FieldButton button : this.buttons) {
			button.update();
		}
	}

}
