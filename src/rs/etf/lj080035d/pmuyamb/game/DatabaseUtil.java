package rs.etf.lj080035d.pmuyamb.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rs.etf.lj080035d.pmuyamb.game.logic.Player.MoveData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseUtil extends SQLiteOpenHelper {
	public static final String GAME_TABLE = "game_table";
	public static final String MOVE_TABLE = "moves";

	public static final String ID = "id";
	public static final String GAME_ID = "game_id";
	public static final String PLAYER = "player";
	public static final String PLAYED_ON = "played_on";
	public static final String GAME_DURATION = "game_duration";
	public static final String SCORE = "score";

	public static final String GAME_PKEY = "score_id";
	public static final String COLUMN = "column";
	public static final String FIELD = "field";
	public static final String VALUE = "value";

	private static final String SQL_CREATE_GAME_TABLE =
			"CREATE TABLE " + GAME_TABLE + " (" +
					DatabaseUtil.ID + " INTEGER PRIMARY KEY," +
					DatabaseUtil.GAME_ID + " INTEGER," +
					DatabaseUtil.PLAYER + " TEXT," +
					DatabaseUtil.PLAYED_ON + " LONG," +
					DatabaseUtil.GAME_DURATION + " LONG," +
					DatabaseUtil.SCORE + " INTEGER" +
					" )";

	private static final String SQL_CREATE_MOVE_TABLE =
			"CREATE TABLE " + MOVE_TABLE + " (" +
					DatabaseUtil.ID + " INTEGER PRIMARY KEY," +
					DatabaseUtil.GAME_PKEY + " INTEGER," +
					DatabaseUtil.COLUMN + " INTEGER," +
					DatabaseUtil.FIELD + " INTEGER," +
					DatabaseUtil.VALUE + " INTEGER" +
					" )";

	private static final String SQL_DELETE_ENTRIES1 =
			"DROP TABLE IF EXISTS " + DatabaseUtil.GAME_TABLE;
	private static final String SQL_DELETE_ENTRIES2 =
			"DROP TABLE IF EXISTS " + DatabaseUtil.MOVE_TABLE;

	public static final int DATABASE_VERSION = 3;
	public static final String DATABASE_NAME = "Yamb.db";

	public DatabaseUtil(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_GAME_TABLE);
		db.execSQL(SQL_CREATE_MOVE_TABLE);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_ENTRIES1);
		db.execSQL(SQL_DELETE_ENTRIES2);
		onCreate(db);
	}

	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

	public static void resetAllScores(Context context) {
		DatabaseUtil dbu = new DatabaseUtil(context);
		SQLiteDatabase db = dbu.getWritableDatabase();
		dbu.onUpgrade(db, DATABASE_VERSION, DATABASE_VERSION);
	}

	public static long insert(Context context, int gamePkey, MoveData move) {
		DatabaseUtil dbu = new DatabaseUtil(context);
		SQLiteDatabase db = dbu.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(GAME_PKEY, gamePkey);
		values.put(COLUMN, move.getColumn().getCode());
		values.put(FIELD, move.getField().getCode());
		values.put(VALUE, move.getValue());

		return db.insert(MOVE_TABLE, null, values);
	}

	public static long insert(Context context, int gameId, String playerName, long playedOn, long duration, int score) {
		DatabaseUtil dbu = new DatabaseUtil(context);
		SQLiteDatabase db = dbu.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(GAME_ID, gameId);
		values.put(PLAYER, playerName);
		values.put(PLAYED_ON, playedOn);
		values.put(GAME_DURATION, duration);
		values.put(SCORE, score);

		return db.insert(GAME_TABLE, null, values);
	}

	public static int getNextGameId(Context context) {
		DatabaseUtil dbu = new DatabaseUtil(context);
		SQLiteDatabase db = dbu.getReadableDatabase();

		Cursor cursor = db.rawQuery("SELECT MAX(" + GAME_ID + ") FROM " + GAME_TABLE, null);
		cursor.moveToFirst();

		try {
			int max = cursor.getInt(0);
			return max + 1;
		} catch (Exception e) {
			return 1;
		}
	}

	public static class GameRow {
		private int id;
		private int gameId;
		private String playerName;
		private long playedOn;
		private long duration;
		private int score;

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getGameId() {
			return gameId;
		}
		public void setGameId(int gameId) {
			this.gameId = gameId;
		}
		public String getPlayerName() {
			return playerName;
		}
		public void setPlayerName(String playerName) {
			this.playerName = playerName;
		}
		public long getPlayedOn() {
			return playedOn;
		}
		public void setPlayedOn(long playedOn) {
			this.playedOn = playedOn;
		}
		public long getDuration() {
			return duration;
		}
		public void setDuration(long duration) {
			this.duration = duration;
		}
		public int getScore() {
			return score;
		}
		public void setScore(int score) {
			this.score = score;
		}
	}

	public static List<GameRow> getAllGameRows(Context context) {
		List<GameRow> gameRows = new LinkedList<GameRow>();

		DatabaseUtil dbu = new DatabaseUtil(context);
		SQLiteDatabase db = dbu.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM " + GAME_TABLE + " ORDER BY " + SCORE + " DESC, " + ID + " DESC", null);

		while (cursor.moveToNext()) {
			GameRow oneRow = new GameRow();

			oneRow.setId(cursor.getInt(cursor.getColumnIndex(ID)));
			oneRow.setGameId(cursor.getInt(cursor.getColumnIndex(GAME_ID)));
			oneRow.setPlayerName(cursor.getString(cursor.getColumnIndex(PLAYER)));
			oneRow.setPlayedOn(cursor.getLong(cursor.getColumnIndex(PLAYED_ON)));
			oneRow.setDuration(cursor.getLong(cursor.getColumnIndex(GAME_DURATION)));
			oneRow.setScore(cursor.getInt(cursor.getColumnIndex(SCORE)));

			gameRows.add(oneRow);
		}

		return gameRows;
	}

	public static ArrayList<MoveData> getAllPlayerMoves(Context context, int pkey) {
		ArrayList<MoveData> moves = new ArrayList<MoveData>();

		DatabaseUtil dbu = new DatabaseUtil(context);
		SQLiteDatabase db = dbu.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM " + MOVE_TABLE + " WHERE " + GAME_PKEY + " = " + pkey + " ORDER BY " + ID + " ASC", null);

		while (cursor.moveToNext()) {
			int column = cursor.getInt(cursor.getColumnIndex(COLUMN));
			int field = cursor.getInt(cursor.getColumnIndex(FIELD));
			int value = cursor.getInt(cursor.getColumnIndex(VALUE));

			moves.add(new MoveData(column, field, value));
		}

		return moves;
	}

}
