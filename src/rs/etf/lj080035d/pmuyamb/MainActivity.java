package rs.etf.lj080035d.pmuyamb;

import rs.etf.lj080035d.pmuyamb.game.ui.PrepareGameActivity;
import rs.etf.lj080035d.pmuyamb.preferences.PreferencesActivity;
import rs.etf.lj080035d.pmuyamb.statistics.StatisticsActivity;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return false;
	}

	public void newGameButtonPressed(View view)
	{
		Intent intent = new Intent(this, PrepareGameActivity.class);
		startActivity(intent);
	}

	public void resultsButtonPressed(View view)
	{
		Intent intent = new Intent(this, StatisticsActivity.class);
		startActivity(intent);
	}

	public void settingsButtonPressed(View view)
	{
		Intent intent = new Intent(this, PreferencesActivity.class);
		startActivity(intent);
	}
}
