package rs.etf.lj080035d.pmuyamb.statistics;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rs.etf.lj080035d.pmuyamb.R;
import rs.etf.lj080035d.pmuyamb.Threading;
import rs.etf.lj080035d.pmuyamb.game.DatabaseUtil;
import rs.etf.lj080035d.pmuyamb.game.DatabaseUtil.GameRow;
import rs.etf.lj080035d.pmuyamb.game.logic.Player.MoveData;
import rs.etf.lj080035d.pmuyamb.game.ui.GameActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

public class StatisticsActivity extends ActionBarActivity {
	
	private LinearLayout scoresLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);
		
		this.scoresLayout = (LinearLayout) findViewById(R.id.resultsLayout);
		update(new LinkedList<GameRow>());
		Threading.getScores(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return false;
	}
	
	public void update(List<GameRow> rows) {
		this.scoresLayout.removeAllViews();
		
		Result result = new Result(getApplicationContext(), null, this);
		this.scoresLayout.addView(result);
		for (GameRow oneRow : rows) {
			result = new Result(getApplicationContext(), oneRow, this);
			this.scoresLayout.addView(result);
		}
	}
	
	public void resetPressed(View view) {
		Threading.resetScores();
		update(new LinkedList<GameRow>());
	}
	
	public void replay(ArrayList<MoveData> moves, GameRow gameRow) {
		Intent intent = new Intent(getApplicationContext(), GameActivity.class);
		intent.putExtra("moves", moves);
		intent.putExtra("playerName", gameRow.getPlayerName());
		startActivity(intent);
	}
}
