package rs.etf.lj080035d.pmuyamb.statistics;

import java.util.Date;

import rs.etf.lj080035d.pmuyamb.MyYambApp;
import rs.etf.lj080035d.pmuyamb.R;
import rs.etf.lj080035d.pmuyamb.Threading;
import rs.etf.lj080035d.pmuyamb.game.DatabaseUtil.GameRow;
import android.content.Context;
import android.text.format.DateFormat;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Result extends LinearLayout {

	public Result(final Context context, final GameRow gameRow, final StatisticsActivity activity) {
		super(context);
		setOrientation(HORIZONTAL);
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		
		if (gameRow == null) {
			setupHeader(context);
			return;
		}
		
		TextView text = new TextView(context);
		text.setText(gameRow.getPlayerName());
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
		
		text = new TextView(context);
		text.setText(Integer.toString(gameRow.getScore()));
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
		
		text = new TextView(context);
		text.setText(DateFormat.getMediumDateFormat(MyYambApp.getContext()).format(new Date(gameRow.getPlayedOn())));
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
		
		text = new TextView(context);
		long mins = gameRow.getDuration() / 1000 / 60;
		long secs = gameRow.getDuration() / 1000 % 60;
		text.setText(mins + ":" + secs);
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
		
		Button button = new Button(context);
		button.setText(context.getString(R.string.simulate));
		button.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(button);
		
		final int key = gameRow.getId();
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Threading.startReplay(activity, gameRow, key);
			}
		});
	}
	
	private void setupHeader(Context context) {
		TextView text = new TextView(context);
		text.setText("Player name:");
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
		
		text = new TextView(context);
		text.setText("Score:");
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
		
		text = new TextView(context);
		text.setText("Played on:");
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
		
		text = new TextView(context);
		text.setText("Played for (m:s):");
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
		
		text = new TextView(context);
		text.setText("Press to simulate");
		text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));
		addView(text);
	}

}
