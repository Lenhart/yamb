package rs.etf.lj080035d.pmuyamb;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Handler;
import rs.etf.lj080035d.pmuyamb.game.DatabaseUtil;
import rs.etf.lj080035d.pmuyamb.game.DatabaseUtil.GameRow;
import rs.etf.lj080035d.pmuyamb.game.logic.Player;
import rs.etf.lj080035d.pmuyamb.game.logic.Player.MoveData;
import rs.etf.lj080035d.pmuyamb.statistics.StatisticsActivity;

public class Threading {

	public static void getScores(final StatisticsActivity activity)
	{
		new Thread(new Runnable() {

			@Override
			public void run() {
				final List<GameRow> rows = DatabaseUtil.getAllGameRows(MyYambApp.getContext());
				Handler handler = new Handler(MyYambApp.getContext().getMainLooper());
				handler.post(new Runnable() {
					
					@Override
					public void run() {
						activity.update(rows);
					}
				});
			}
		}).start();
	}

	public static void resetScores()
	{
		new Thread(new Runnable() {

			@Override
			public void run() {
				DatabaseUtil.resetAllScores(MyYambApp.getContext());
			}
		}).start();
	}

	public static void startReplay(final StatisticsActivity activity, final GameRow gameRow, final int key)
	{
		new Thread(new Runnable() {

			@Override
			public void run() {
				final ArrayList<MoveData> moves = DatabaseUtil.getAllPlayerMoves(MyYambApp.getContext(), key);
				Handler handler = new Handler(MyYambApp.getContext().getMainLooper());
				handler.post(new Runnable() {
					
					@Override
					public void run() {
						activity.replay(moves, gameRow);
					}
				});
			}
		}).start();
	}

	public static void storeGame(final List<Player> players, final long duration, final long gameStarted)
	{
		new Thread(new Runnable() {

			@Override
			public void run() {
				Context context = MyYambApp.getContext();
				int gameId = DatabaseUtil.getNextGameId(context);
				for (Player player : players) {
					int key = (int) DatabaseUtil.insert(context, gameId, player.getName(), gameStarted, duration, player.getScore());
					player.dump(context, key);
				}
			}
		}).start();
	}

}
